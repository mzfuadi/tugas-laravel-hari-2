<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OTP extends Model
{
    protected $table = 'OTP';
    


    public function otp()
    {
        return $this->belongsTo('App\OTP','role_id');
    }

    protected static function boot(){
        static::creating(function ($model){
            if( ! $model0>getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing(){
        return false;
    }

    public function getKeyType(){
        return 'string';
    }
}
