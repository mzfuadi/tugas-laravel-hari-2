<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'Roles';
    
    public function role()
    {
        return $this->belongsTo('App\Role','user_id');
    }

    public function otp()
    {
        return $this->hasOne('app\OTP','role_id');
    }

    protected static function boot(){
        static::creating(function ($model){
            if( ! $model0>getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing(){
        return false;
    }

    public function getKeyType(){
        return 'string';
    }
}
